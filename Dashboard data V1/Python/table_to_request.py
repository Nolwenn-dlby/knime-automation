from pandas import DataFrame
# Create empty table


from datetime import datetime, timedelta
import psycopg2
import logging
import time
import calendar


def connect_postgres(postgres_conf):
    conn = psycopg2.connect(postgres_conf)
    cur = conn.cursor()
    return cur, conn


class PostgresqlConnection(object):
    def __init__(self, host, port, user, password, dbname):
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.dbname = dbname

    def str_format(self):
        return "host=%s port=%s user=%s password=%s dbname=%s" % (
        self.host, self.port, self.user, self.password, self.dbname)


postgres = {
    "nolwenn": {
        "ds": PostgresqlConnection("127.0.0.1", "5001", "etudes", "jw8s0F4", "ds03"),

    }}


def get_date_inf(x):
    inf = (x.day // 5) * 5
    if inf == 30:
        inf = 25
    elif inf == 0:
        inf = 1
    return x.replace(day=inf)


def get_date_sup(x):
    sup = (x.day // 5 + 1) * 5
    if sup > 25:
        sup = 1
        x += timedelta(days=7)
    return x.replace(day=sup)


def gen_5days_dates(begin, end):
    begin_date = datetime.strptime(begin, '%Y%m%d')
    end_date = datetime.strptime(end, '%Y%m%d')
    if begin_date > end_date:
        logging.warning('Problem generating the list of tables : begin_date < end_date')
        return []
    try:
        date_list = []
        x0, y0 = get_date_inf(begin_date), get_date_sup(begin_date)
        date_list.append("%s_%s" % (x0.strftime('%Y%m%d'), y0.strftime('%Y%m%d')))
        while end_date >= y0:
            tmp = get_date_sup(y0)
            date_list.append("%s_%s" % (y0.strftime('%Y%m%d'), tmp.strftime('%Y%m%d')))
            y0 = tmp
        return date_list
    except Exception as e:
        logging.warning(e)
        return []


def list_tables_like(in_conn, in_cur, in_dbname, in_pg_schema, in_tablename):
    in_cur.execute("SELECT table_schema||'.'||table_name "
                   "FROM information_schema.tables "
                   "WHERE table_catalog = '{}' "
                   "  AND table_schema = '{}' "
                   "  AND table_name LIKE '{}' "
                   "  AND table_type = 'BASE TABLE'"
                   "  ORDER BY 1".format(in_dbname, in_pg_schema, in_tablename))
    sql_results = in_cur.fetchall()
    tables = [x[0] for x in sql_results]
    in_conn.commit()
    return tables


def extract_tables(cur_ds, conn_ds, begin_date, end_date):
    """
    Find list of tables to use for study
    :param cur_ds: cursor of ds postgres connection
    :param conn_ds: ds postgres connection
    :param begin_date: beginning of study (2017-02-01)
    :param end_date: end of study (2017-02-02)
    :return: list<string> (pg_schema.table)
    """
    list_5days_dates = gen_5days_dates(begin_date, end_date)
    list_postgres_tables = []
    for days in list_5days_dates:
        for name in ['bid', 'no_bid']:
            for pg_schema in ['ds02', 'ds03']:
                list_postgres_tables.extend(
                    list_tables_like(conn_ds, cur_ds, 'ds03', pg_schema, 'v3_{}_{}%'.format(name, days)))
    return list_postgres_tables


cur_ds, conn_ds = connect_postgres(postgres['nolwenn']['ds'].str_format())

month_brut = str(flow_variables['month'])
print(len(month_brut))

len_month = len(month_brut)

if len_month == 1:
    month = "0%s" % (month_brut)
else:
    month = smonth_brut

year = str(flow_variables['year'])
last_day = str(calendar.monthrange(flow_variables['year'], flow_variables['month'])[1])

first = "%s%s01" % (year, month)
second = "%s%s%s" % (year, month, last_day)

output_table = DataFrame(extract_tables(cur_ds, conn_ds, first, second))



